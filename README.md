# Unit Testing (Jest + Enzyme)
Unit testing is a methodology where units of code are tested in isolation from the rest of the application. The Unit is the smallest piece of code that testable separately from the whole application. A unit test might test a particular function, object, class, or module. Unit tests are great to test whether or not individual parts of application work. But unit tests don’t test whether or not units work together when they’re composed to form a whole application.
> **Suggestion**: Tests that do these things add value. They should be part of your general approach to code quality.

## Jest 
Jest is test framework supported by default by React. It provide test runner & assertion library. [Jest]([https://jestjs.io/docs/en/expect])
## Enzyme
Enzyme lets you simulate DOM rendering and actions on the components. It integrates well with Jest. [Enzyme]([https://jestjs.io/docs/en/expect]) [Jest-Enzym]([https://github.com/FormidableLabs/enzyme-matchers/tree/master/packages/jest-enzyme#assertions])

