import React from 'react';
import classNames from 'classnames';

import './alert.scss';

const Alert = props => {
  /* prettier-ignore */
  const {
    theme,
    children,
    className,
    size,
    ...attributes
  } = props;

  const classes = classNames(
    `Alert`,
    {
      [`Alert--${theme}`]: theme,
      [`u-pd-${size}`]: size,
    },
    className,
  );

  return (
    <div className={classes} {...attributes}>
      {children}
    </div>
  );
};

export default Alert;
