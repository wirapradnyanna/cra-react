import React from "react";
import { shallow } from "enzyme";
import Alert from "./alert";

// EXAMPLE 2: Test a presentational component

describe("<Alert/>", () => {
  it("render correctly", () => {
    const wrapper = shallow(<Alert />);
    expect(wrapper.is(".Alert")).toBeTruthy();
  });

  it("render correct theme", () => {
    const wrapper = shallow(<Alert theme="success" />);
    expect(wrapper.is(".Alert.Alert--success")).toBeTruthy();
  });

  it("render correct size", () => {
    const wrapper = shallow(<Alert size="base" />);
    expect(wrapper.is(".Alert.u-pd-base")).toBeTruthy();
  });

  it("render correct children", () => {
    const wrapper = shallow(<Alert>This is alert</Alert>);
    expect(wrapper.text()).toBe("This is alert");
  });
});
