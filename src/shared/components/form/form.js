import React, { memo, useEffect } from 'react';
import classNames from 'classnames';
import { connect } from 'formik';

const Form = memo(props => {
  /* prettier-ignore */
  const { 
    tag: Tag = 'form', 
    children, 
    className, 
    formik,
    enableOnEnter,
    ...attributes
  } = props;

  const classes = classNames(`Form`, className);

  const handleKeyDown = keyEvent => {
    const isEnter = (keyEvent.charCode || keyEvent.keyCode) === 13;

    if (isEnter && !enableOnEnter) {
      keyEvent.preventDefault();
    }
  };

  const validateForm = () => {
    if (formik && formik.validateForm && formik.setErrors) {
      formik.validateForm().then(() => {
        formik.setErrors({});
      });
    }
  };

  useEffect(() => {
    validateForm();

    return () => {};
  }, []);

  return (
    <Tag onKeyDown={handleKeyDown} {...attributes} className={classes}>
      {children}
    </Tag>
  );
});

export default connect(Form);
