import React from "react";
import { Formik } from "formik";
import Form from "./form";
import SearchIcon from "./search.svg";

const SearchField = (props) => {
  const { placeholder, fieldId, handleSearch: onSearch } = props;

  const handleSearch = (values) => {
    onSearch && onSearch(values.search);
  };

  return (
    <Formik initialValues={{ search: "" }} onSubmit={handleSearch}>
      {({ values, handleChange, handleSubmit }) => (
        <Form onSubmit={handleSubmit}>
          <div className="form__group">
            <div className="search-field action__group">
              <input
                id={fieldId}
                name="search"
                value={values.search}
                onChange={handleChange}
                className="form__control"
                placeholder={placeholder}
              />
              <button
                id="BtnSearch"
                type="submit"
                className="Button Button--light action__icon u-Flex u-pd-xsmall u-bg-light u-pd-right-base u-pd-left-base"
                style={{ border: "1px solid #ccc" }}
              >
                <img src={SearchIcon} alt="Search" />
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default SearchField;
