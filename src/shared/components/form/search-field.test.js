import React from "react";
import { act } from "react-dom/test-utils";
import { mount, shallow } from "enzyme";
import SearchField from "./search-field";

// EXAMPLE 3: Test a component with user event

describe("<SearchField/>", () => {
  it("render correctly", () => {
    const wrapper = shallow(<SearchField />);
    expect(wrapper.find(".search-field")).toBeTruthy();
  });

  it("search button trigger search handle properly", () => {
    const handleSearch = jest.fn();
    const wrapper = mount(<SearchField handleSearch={handleSearch} />);
    act(() => {
      // handleSearch("");
      wrapper.find("button").simulate("click");
    });
    expect(handleSearch).toHaveBeenCalledWith("");
  });
});
