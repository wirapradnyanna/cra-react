import { createContext } from 'react';

const SnackbarContext = createContext({
  visible: false,
  openSnackbar: () => null,
  closeSnackbar: () => null,
});

export default SnackbarContext;
