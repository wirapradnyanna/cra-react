import React, { useState, useEffect } from "react";
import classNames from "classnames";
import SnackbarContext from "./snackbar.context";

/**
 * @name SnackbarProvider
 * @desc A context provider for the snackbar component.
 * @param {*} param children | Pass children prop.
 * @return <SnackbarContext.Provider />
 */
export const SnackbarProvider = (props) => {
  const { children } = props;

  const [value, setValue] = useState("");
  const [visible, setVisibility] = useState(false);
  const [timeouts, setTimeouts] = useState([]);
  const [ref, setRef] = useState(null);

  const openSnackbar = (obj) => {
    // Hide previously instantiated snackbar to display the next snackbar.
    if (visible) {
      closeSnackbar();
    }

    setValue(obj.value);

    // Clear timeouts before displaying the next snackbar.
    clearTimeouts();
    setTimeouts([...timeouts, setTimeout(() => setVisibility(true), 500)]);
  };

  const beforeClosing = () => {
    if (ref && ref.current) {
      ref.current.className = classNames("Snackbar", "is-hiding");
    }
  };

  const closeSnackbar = () => {
    beforeClosing();

    return setTimeouts([
      ...timeouts,
      setTimeout(() => setVisibility(false), 250),
    ]);
  };

  const clearTimeouts = () => {
    // Remove concurrent timeout instances.
    timeouts.forEach((timeout, i) => clearTimeout(timeout[i]));

    for (var i = 0; i < timeouts.length; i++) {
      clearTimeout(timeouts[i]);
    }

    // Clean timeout local state.
    setTimeouts([]);
  };

  useEffect(() => {
    if (visible) {
      setTimeouts([...timeouts, setTimeout(() => closeSnackbar(), 5000)]);

      if (ref) {
        ref.current.className = classNames("Snackbar", "is-opening");
      }
    }
  }, [visible, ref]);

  return (
    <SnackbarContext.Provider
      value={{
        visible,
        openSnackbar,
        closeSnackbar,
        setRef,
        value,
      }}
    >
      {children}
    </SnackbarContext.Provider>
  );
};

export default SnackbarProvider;
