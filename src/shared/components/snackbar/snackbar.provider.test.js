import React, { useContext } from "react";
import { act } from "react-dom/test-utils";
import { mount } from "enzyme";
import SnackbarContext from "./snackbar.context";
import SnackbarProvider from "./snackbar.provider";

// EXAMPLE 4: Test a provider component

const TestComponent = () => {
  const { value, visible, openSnackbar, closeSnackbar } = useContext(
    SnackbarContext
  );
  return (
    <div className="TestComponent">
      <div id="value">{value}</div>
      <div id="visible">{visible ? "opened" : "closed"}</div>
      <button id="openSnackbar" onClick={() => openSnackbar({ value: "Test" })}>
        Open
      </button>
      <button id="closeSnackbar" onClick={closeSnackbar}>
        Close
      </button>
    </div>
  );
};

beforeEach(() => {
  jest.useFakeTimers();
});

describe("<SnackbarProvider/>", () => {
  describe("render inital value correctly", () => {
    const wrapper = mount(
      <SnackbarProvider>
        <TestComponent />
      </SnackbarProvider>
    );

    it("render visible closed", () => {
      expect(wrapper.find("#visible").text()).toBe("closed");
    });
    it("render value empty string", () => {
      expect(wrapper.find("#value").text()).toBe("");
    });
  });

  it("openSnackbar correctly", () => {
    const wrapper = mount(
      <SnackbarProvider>
        <TestComponent />
      </SnackbarProvider>
    );

    act(() => {
      wrapper.find("#openSnackbar").simulate("click");
      jest.runAllTimers();
    });

    expect(wrapper.find("#value").text()).toBe("Test");
    expect(wrapper.find("#visible").text()).toBe("opened");
  });

  it("closeSnackbar correctly", () => {
    const wrapper = mount(
      <SnackbarProvider>
        <TestComponent />
      </SnackbarProvider>
    );

    act(() => {
      wrapper.find("#openSnackbar").simulate("click");
      jest.runAllTimers();
      wrapper.find("#closeSnackbar").simulate("click");
      jest.runAllTimers();
    });

    expect(wrapper.find("#visible").text()).toBe("closed");
  });
});
