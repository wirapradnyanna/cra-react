// Turn phone number 08xxx to +62xxx
export const normalizePhoneNumber = (phoneNumber = "") => {
  if (!/^(08)/.test(phoneNumber)) {
    throw new Error("Invalid phone number");
  }
  const normalizedPhoneNumber = phoneNumber.substr(phoneNumber.indexOf("8")); // get the value from first string to string in the position index of '8', include the '8'
  return `+62${normalizedPhoneNumber}`;
};
