import { normalizePhoneNumber } from "./phone-number.util";

// EXAMPLE 1: Test utility function

describe("normalizePhoneNumber", () => {
  it("normalize a valid phone number", () => {
    const phoneNumber = normalizePhoneNumber("08123456789");
    expect(phoneNumber).toBe("+628123456789");
  });

  it("throw error for invalid phone number that not started with 08", () => {
    expect(() => normalizePhoneNumber("8123456789")).toThrow(
      "Invalid phone number"
    );
    // expect(() => normalizePhoneNumber('8123456789')).toThrow(); // if you won't test thrown error message
  });

  it("throw error for empty string", () => {
    expect(() => normalizePhoneNumber("")).toThrow();
  });
});
